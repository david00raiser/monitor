from email.message import EmailMessage
from email.headerregistry import Address
import smtplib

class Mailer:
    'Class for mailing results'
    data = {}
    def __init__(self, body, subject=''):
        from global_config import mailing_ulogin,mailing_plogin,mailing_server,mailing_port,mailing_subject,mailing_from_address,mailing_to_address
        self.data['mailing_ulogin'] = mailing_ulogin
        self.data['mailing_plogin'] = mailing_plogin
        self.data['mailing_server'] = mailing_server
        self.data['mailing_port'] = mailing_port
        self.data['mailing_subject'] = mailing_subject
        self.data['mailing_from_address'] = mailing_from_address
        self.data['mailing_body'] = body
        addresses = list()
        for item in mailing_to_address:
            addresses.append(Address(
                display_name = (item['email']) if not item.get("name") else item['name'],
                username = item['email'].split('@')[0],
                domain = item['email'].split('@')[1])
            )
        self.data['mailing_to_address'] = addresses

    def create_email_message(self, from_address, to_address, subject, body):
        try:
            msg = EmailMessage()
            msg['From'] = from_address
            msg['To'] = to_address
            msg['Subject'] = subject
            msg.set_content(body)
            return msg
        except:
            print("Error at creating EmailMessage object")

    def sender(self):
        msg = self.create_email_message(
            from_address = self.data['mailing_from_address'],
            to_address = self.data['mailing_to_address'],
            subject = self.data['mailing_subject'],
            body = self.data['mailing_body']
        )

        try:
            with smtplib.SMTP(self.data['mailing_server'], port=self.data['mailing_port']) as smtp_server:
                smtp_server.ehlo()
                smtp_server.starttls()
                smtp_server.login(self.data['mailing_ulogin'], self.data['mailing_plogin'])
                smtp_server.send_message(msg)
        except:
            print("Error at SMTP mail sending")