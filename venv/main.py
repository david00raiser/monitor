from mailer import Mailer
from exceptions import CustomException
from xml_parser import XMLParser
from model import DBModel
from Job import Job
from bs4 import BeautifulSoup as BS
from anytree import Node, RenderTree
import pprint

def curl(url, params, type):
    type = type.lower()
    try:
        import requests
        if type=="get":
            response = requests.get(url, params)
        elif type=="post":
            response = requests.post(url, params)
    except:
        raise CustomException("Network problem at CURL")
    else:
        if response.status_code != 200:
            return -1, None
        else:
            html = BS(response.text, 'html.parser')  # parseador
            list = html.find('table', attrs={'id': 'tblEjec'}).findChildren('tr')
            list.pop(0) # remove table header
            return 0, list

def process(url, params, type, sysout=None):
    rc, rawdata = curl(url, params,type)
    if rc == 0:
        if len(rawdata) > 0:  # ok
            data = []
            for item in rawdata:
                list = item.findAll("td")
                json = {}
                for elem in list:
                    json[elem.get("data-title")] = elem.text
                    if elem.get("data-title") =='Sysout': #and sysout:
                        sysout_link = elem.find("input")["onclick"].replace('window.open("','').replace('");','')
                        json[elem.get("data-title")] = sysout_link
                data.append(json)
            return data
    else:
        return rc # problema en curl

def make_active_message(input_data):
    message = ''
    if input_data:
        #data = sorted(input_data, key=lambda k: k['Status'])
        data = sorted(input_data, key=lambda k: "%s %s" % (k['Status'], k['Jobname']))

        for item in data: #iterate elements
            if item["Status"] != 'Ended OK' and item["Odate"]=='181221':
                message += item["Odate"]+" "+item["Status"].replace("¿Que espera para ejecutar?", "")+ "\t"+str(Job(item['Jobname']))+"\n"
    else:
        message = '0'
    return message

def get_status_active():
    from global_config import folders, jobs_list, folders, active_url
    full_data=[]
    filters = []
    type = 'GET'
    filters += folders
    filters += jobs_list

    if len(filters) > 0:
        for filter in filters:
            rs = process(active_url, {"filtro": filter}, type)
            if rs != -1:
                if rs != None:
                    full_data += rs
            else:
                raise CustomException("Problem in target server at curl'ing:", rs)

        body = make_active_message(full_data)
        print(body)
        #mailer = Mailer(body)
        #mailer.sender()
    else:
        print("No jobs in params to analyze!")

def initial_load_xmlfiles():
    parser = XMLParser()
    parser.scan_files()
    files = parser.XMLfiles

    if len(files)>0:
        if parser.analyze_files():
            folders = parser.folders
            jobs = parser.jobs
            for i in files: print("Parsed file:{}".format(i))

            try:
                from global_config import folder_collection, job_collection
                model = DBModel()
                model.remove(folder_collection)
                model.remove(job_collection)
                model.save(folder_collection, folders)
                model.save(job_collection, jobs)
            except:
                raise CustomException("Problem at comunicating with Cluster")

def dp(job, offset = 0, repeat = False):
    if (job.name[1:4] in uuaas): #valid uuaa & DP
        #print("{}{} - {}".format('\t' * offset,job.name, job.description))
        if len(job.get_parents())==0:
            return

        if (job.name in mem_parents.keys()) == False:
            mem_parents[job.name] = job.parents

            for parent in job.parents:
                arr_tree[parent.name] = Node("{} - {}".format(parent.name, parent.description), parent = arr_tree[job.name])
                dp(parent, offset+1)

def print_tree(head):
    for pre, fill, node in RenderTree(arr_tree[head]):
        print("%s%s" % (pre, node.name))

def get_tree():
    global mem_parents
    global uuaas
    global offset
    global arr_tree
    arr_tree = {}
    offset = 0
    mem_parents = {}
    uuaas = [ "IDS","CDO","DWP"]

    job_head = "PIDSCP0283"
    myjob = Job(job_head)
    arr_tree[job_head] = Node("{} - {}".format(myjob.name, myjob.description))
    dp(myjob)
    print_tree(job_head)

    #pprint.pprint(mem_parents)

def load_logs():
    from global_config import folders, jobs_list, active_url
    from datetime import datetime

    # url1 = 'http://150.100.216.64:8080/scheduling/ejecucionesConsulta'
    # param1 = {'txtFromDate': '2018-12-20',
    #           'txtToDate': '2018-12-21',
    #           'jobname': 'PDCOPP0040',
    #           'nodo': ''}
    date_begin_str = '2018-11-01'
    date_end_str = '2018-12-21'
    job = 'PDCOPP0040'

    url2 = 'http://150.100.216.64:8080/scheduling/ejecucionesStatusConsulta'
    param2 = {'txtFromDate': date_begin_str,
              'txtToDate': date_end_str,
              'txtFromTime': '00:00',
              'txtToTime': '23:59',
              'jobname': job,
              'foldername': '',
              'aplicacion': '',
              'nodeid': ''}
    rawdata = process(url2, param2, 'POST', 1)
    #pprint.pprint(rawdata)

    date_begin = datetime.strptime("{}T00:00:00.000Z".format(date_begin_str), "%Y-%m-%dT%H:%M:%S.000Z")
    date_end = datetime.strptime("{}T23:59:00.000Z".format(date_end_str), "%Y-%m-%dT%H:%M:%S.000Z")

    model = DBModel()
    print(date_begin)
    print(date_end)
    #model.remove(logs_collection, filters)

if __name__ == '__main__':
    get_status_active()
    #initial_load_xmlfiles()
    #get_tree()
    load_logs()

#References
#https://alysivji.github.io/sending-emails-from-python.html
#https://www.crummy.com/software/BeautifulSoup/bs4/doc/#the-recursive-argument

#FIN DE MALLA(ULTIMO JOB) X ODATE
#verificar job caido sysout