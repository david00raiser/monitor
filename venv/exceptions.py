class CustomException(Exception):
    pass

class ValidationError(Exception):
    def __init__(self, message, errors):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)

        #custom codes...
        self.errors = errors