odate = []
jobs_list = ['PDCOCP0086', 'PDCOCP0085', 'PDCOPP0045', 'PDCOCP0084', 'PDCOCP0088', 'PDCOPP0043', 'PDCOCP0087',
               'PDCOPP0042', 'PDCOCP0120', 'PDCOCP0126', 'PDCOPP0018', 'PDCOCP0125', 'PDCOPP0017', 'PDCOCP0046',
               'PDCOCP0077', 'PDCOCP0079', 'PDCOPP0023', 'PDCOCP0078', 'PDCOPP0022', 'PDCOCP0112', 'PDCOCP0115',
               'PDCOPP0047', 'PDCOCP0124', 'PDCOCP0127', 'PDCOPP0031', 'PDCOCP0053', 'PDCOCP0050', 'PDCOCP0094',
               'PDCOCP0095', 'PDCOPP0040']
folders= ["CR-PEDWPDIA-T02","CR-PECDODIA-T02","CR-PEDWPMEN-T02","CR-PECDOMEN-T02","CR-PEIDSDIA-T02"]
folders_path = 'folders'
daily_xml_folders = []
monthly_xml_folders = []
active_url = 'http://150.100.216.64:8080/scheduling/ajF'
sysout_query_url = 'http://150.100.216.64:8080/scheduling/sysoutsConsulta'
sysout_url = ''
logs_url = ''

#Mailing config : Just to notify cancelled processes
mailing_ulogin='david.huaman@bluetab.net'
mailing_plogin=''
mailing_server='smtp.gmail.com'
mailing_port=587
mailing_subject='PROCESOS PENDIENTES'
mailing_from_address='david.huaman@bluetab.net'
mailing_to_address=[
    {'email':'david.huaman.contractor@bbva.com','name': 'DAVID ARTURO HUAMAN ÑIQUEN'}
    ,{'email':'dwp-datos.group@bbva.com'}
]

#Database configs
db_name ='model'
folder_collection = 'folder'
job_collection = 'job'
logs_collection = 'logs'