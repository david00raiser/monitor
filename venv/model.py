from exceptions import CustomException
#import pymongo

from pymongo import MongoClient

class MongoCon(object):
    'Class for CRUDing in mongoDB'
    __db = None

    @classmethod
    def get_connection(cls):
        if cls.__db is None:
            user = 'app1'
            passw = 'urLXhX3B6St4i7N'
            db = 'model'
            uri = "mongodb://{}:{}@cluster0-shard-00-00-hptkw.mongodb.net:27017,cluster0-shard-00-01-hptkw.mongodb.net:27017,cluster0-shard-00-02-hptkw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true"
            #cls.__db = MongoClient(uri.format(user, passw))[db]
            cls.__db = MongoClient('mongodb://localhost:27017/')[db]
        return cls.__db

class DBModel (MongoCon):

    def __init__(self):
        self.client = MongoCon.get_connection()

    def save(self, collection, data):
        if isinstance(data, list):
            self.client[collection].insert_many(data).inserted_ids
        else: #is simple dictionary/document
            self.client[collection].insert_one(data)
            #return True if idx != None else False

    def remove(self, collection, filter = None):
        if filter:
            self.client[collection].remove(filter)
        else:
            self.client[collection].remove({})

    def get_job(self, jobname):
        return self.client.job.find_one({"JOBNAME":jobname}, { "_id": 0})


